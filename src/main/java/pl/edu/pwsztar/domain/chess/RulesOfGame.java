package pl.edu.pwsztar.domain.chess;

import org.h2.bnf.Rule;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

public interface RulesOfGame {

    /**
     * Metoda zwraca true, tylko gdy przejscie z polozenia
     * (xStart, yStart) na (xEnd, yEnd) w jednym ruchu jest zgodne
     * z zasadami gry w szachy
     */
    boolean isCorrectMove(int xStart, int yStart, int xEnd, int yEnd);

    @Component
    @Qualifier("Bishop")
    class Bishop implements RulesOfGame {

        @Override
        public boolean isCorrectMove(int xStart, int yStart, int xEnd, int yEnd) {
            if(xStart == xEnd && yStart == yEnd) {
                return false;
            }

            return Math.abs(xEnd - xStart) == Math.abs(yEnd - yStart);
        }
    }

    @Component
    @Qualifier("Knight")
    class Knight implements RulesOfGame {

        @Override
        public boolean isCorrectMove(int xStart, int yStart, int xEnd, int yEnd) {
            if (xStart == xEnd && yStart == yEnd) {
                return false;
            }
            int axisX = Math.abs(xStart-xEnd);
            int axisY = Math.abs(yStart-yEnd);
            return ((axisX == 1 && axisY == 2) || (axisX == 2 && axisY == 1));
        }
    }
    @Component
    @Qualifier("King")
    class King implements RulesOfGame {

        @Override
        public boolean isCorrectMove(int xStart, int yStart, int xEnd, int yEnd) {
            if (xStart == xEnd && yStart == yEnd) {
                return false;
            }
            int axisX = Math.abs(xStart-xEnd);
            int axisY = Math.abs(yStart-yEnd);
            return ((axisX == 0 && axisY == 1) || (axisX == 1 && axisY == 1) || (axisX == 1 && axisY == 0));
        }
    }
    @Component
    @Qualifier("Queen")
    class Queen implements RulesOfGame {

        @Override
        public boolean isCorrectMove(int xStart, int yStart, int xEnd, int yEnd) {
            if (xStart == xEnd && yStart == yEnd) {
                return false;
            }
            if(Math.abs(xEnd - xStart) == Math.abs(yEnd - yStart)){
                return true;
            }
            return (xStart == xEnd && yStart != yEnd) || (xStart != xEnd && yStart == yEnd);
        }
    }
    @Component
    @Qualifier("Rook")
    class Rook implements RulesOfGame {

        @Override
        public boolean isCorrectMove(int xStart, int yStart, int xEnd, int yEnd) {
            if (xStart == xEnd && yStart == yEnd) {
                return false;
            }else {
                return (xStart == xEnd && yStart != yEnd) || (xStart != xEnd && yStart == yEnd);
            }
        }
    }
    @Component
    @Qualifier("Pawn")
    class Pawn implements RulesOfGame {

        @Override
        public boolean isCorrectMove(int xStart, int yStart, int xEnd, int yEnd) {
            if (xStart == xEnd && yStart == yEnd) {
                return false;
            }
            int axisX = Math.abs(xStart-xEnd);
            int axisY = Math.abs(yStart-yEnd);
            return (axisX == 0 && axisY == 1) || (axisX == 0 && axisY == 2);
        }
    }




    // TODO: Prosze dokonczyc implementacje kolejnych figur szachowych: Knight, King, Queen, Rock, Pawn
    // TODO: Prosze stosowac zaproponowane nazwy klas !!! (Prowadzacy zajecia posiada wlasne testy)
    // TODO: Kazda klasa powinna implementowac interfejs RulesOfGame
}
